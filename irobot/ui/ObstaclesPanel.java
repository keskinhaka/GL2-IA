package irobot.ui;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import irobot.logic.Obstacle;
import irobot.logic.Robot;
import irobot.logic.RobotOne;
import irobot.logic.RobotTwo;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.DefaultListModel;


public class ObstaclesPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private DefaultListModel<Obstacle> obstaclesList;
	private JList<Obstacle> obstaclesView;
	
	private DefaultListModel<Robot> robotsList;
	private JList<Robot> robotsView;
	
	public Obstacle getCurrentObstacle() {
		final int idx = obstaclesView.getSelectedIndex();
		return idx == -1 ? null : obstaclesList.get(idx);
	}
	
	public Robot getCurrentRobot() {
		final int idx = robotsView.getSelectedIndex();
		return idx == -1 ? null : robotsList.get(idx);
	}
	
	public ObstaclesPanel() {
		initialize();
	}
	
	private void initialize() {
		ObstaclesPanel that = this;
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0; c.fill = GridBagConstraints.HORIZONTAL;
		
		JScrollPane robotsPane = new JScrollPane();
		robotsList = new DefaultListModel<Robot>();
		robotsList.addElement(new Robot());
		robotsList.addElement(new RobotOne());
		robotsList.addElement(new RobotTwo());
		robotsView = new JList<Robot>(robotsList);
		robotsView.setSelectedIndex(0);
		robotsPane.setPreferredSize(new Dimension(120, 120));
		robotsPane.setViewportView(robotsView);
		add(robotsPane, c);
		
		obstaclesList = new DefaultListModel<Obstacle>();
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, c);
		obstaclesView = new JList<Obstacle>(obstaclesList);
		scrollPane.setPreferredSize(new Dimension(120, 700));
		scrollPane.setViewportView(obstaclesView);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.DARK_GRAY);
		add(buttonPanel, c);

		buttonPanel.setLayout(new GridBagLayout());
		
		JButton btnEditObstacle = new JButton("\u00C9diter obstacle");
		btnEditObstacle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				ObstacleEditor.open(that,  that.getCurrentObstacle());
			}
		});
		btnEditObstacle.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.add(btnEditObstacle, c);
		
		JButton btnDeleteObstacle = new JButton("Supprimer obstacle");
		btnDeleteObstacle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				that.removeObstacle(that.getCurrentObstacle());
			}
		});
		btnDeleteObstacle.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.add(btnDeleteObstacle, c);
		
		JButton btnNewObstacle = new JButton("Nouvel obstacle");
		btnNewObstacle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				ObstacleEditor.open(that, null);
			}
		});
		btnNewObstacle.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.add(btnNewObstacle, c);
		
		loadList();
	}
	
	//Les obstacles sont identifiés par un nom unique
	public void addObstacle(Obstacle o) {
		if(o.getName().matches("^ *$")) {
			JOptionPane.showMessageDialog(this, "Le nom est vide");
			return;
		}
		
		for(int i = 0; i < obstaclesList.size(); i++) {
			if(obstaclesList.get(i).getName().equals(o.getName())) {
				obstaclesList.set(i, o);

				ecritureTab(o.getName(), o.shape(), i);
				return;
			}
		}
		obstaclesList.addElement(o);
		ecritureTab(o.getName(), o.shape(), obstaclesList.size()-1);
	}
	
	public Obstacle findObstacle(String name) {
		for(int i = 0; i < obstaclesList.size(); i++) {
			final Obstacle o = obstaclesList.get(i);
			if(o.getName().equals(name)) return o;
		}
		return null;
	}
	
	public void removeObstacle(Obstacle o) {
		obstaclesList.removeElement(o);
		saveList();
	}
	
	public int count() {
		return obstaclesList.size();
	}
	
	private void saveList() {
		final File[] ff = new File("src/obstacles/").listFiles();
		for(File f : ff) f.delete();
		
		for(int i = 0; i < obstaclesList.size(); i++) {
			final Obstacle o = obstaclesList.getElementAt(i);
			ecritureTab(o.getName(), o.shape(), i);
		}
	}
	
	private void loadList() {
		final File[] ff = new File("src/obstacles/").listFiles();
		Arrays.sort(ff);
		final int nombreObstacles = ff.length;
		
		for(int i=0; i<nombreObstacles; i++){
			obstaclesList.addElement(chargerObstacle(ff[i]));
		}
	}
	
	public static Obstacle chargerObstacle(File f){
		boolean[][] shape = null;
		int largeur;
		int hauteur;
		String nom;
		String line;
		Obstacle LoadinObstacle = new Obstacle(null);

		try {
			FileReader fr = new FileReader (f);
			BufferedReader br = new BufferedReader (fr);	
			try {
				nom = br.readLine();
				line = br.readLine();
				largeur = Integer.parseInt(line);
				line = br.readLine();
				hauteur = Integer.parseInt(line);
				
				shape = new boolean[hauteur][largeur];
				
				for(int i = 0 ; i< hauteur ; i++)
				{
					line = br.readLine();
					int j=0;
					for(int k = 0 ; k<line.length()-1 ; k++)
					{
						if(line.charAt(k) != ' ')
						{
							int temp=0;
							do { temp++; } while(line.charAt(k+temp)!=' ');
							
							if(line.substring(k, k+temp).equals("1"))
								shape[i][j] = true;
							else
								shape[i][j] = false;
							
							j++;
							k+=temp-1;
						}	
					}
				}
				

				br.close();
				fr.close();

				LoadinObstacle = new Obstacle(shape,nom);
				
			} catch (IOException exception) {
				System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
			}
		} catch (FileNotFoundException exception) {
			System.out.println ("L'obstacle n'existe pas.");
		}
	
		return LoadinObstacle;
	}
	
	public static void ecritureTab (String nom, boolean[][]tab1,  int numero){
		File f = new File ("src/obstacles/Obstacle" + numero +".txt" );
	
		try
		{
			FileWriter fw = new FileWriter (f);
			fw.write (nom); 
			fw.write ("\r\n");
			fw.write (String.valueOf(tab1[0].length)); 
			fw.write ("\r\n");
			fw.write (String.valueOf(tab1.length)); 
			fw.write ("\r\n");
			
			for(int d = 0 ; d< tab1.length ; d++)
			{
				for(int e = 0 ; e< tab1[d].length ; e++)
				{
					
					if(tab1[d][e])
						fw.write ("1 ");
					else
						fw.write ("0 ");
						
				}
				fw.write ("\r\n");
			}

			fw.close();
			System.out.println("Obstacle "+ numero +" enregistré.");
		} catch (IOException exception) {
			System.out.println ("Erreur lors de l'écriture : " + exception.getMessage());
		}
	}
}
		

