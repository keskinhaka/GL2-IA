package irobot.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;

import irobot.logic.Obstacle;
import irobot.logic.Point;

public class GridCanvas extends JComponent {
	private static final long serialVersionUID = 1L;
	
	protected int w, h;
	protected int tile_sz;
	
	public GridCanvas() {
		super();
		obstacles = new LinkedList<Obstacle>();
		
		tile_sz = 20;
		w = 10; h = 10;
		setOpaque(true);
	}
	
	protected void initialize() {
		this.setPreferredSize(new Dimension((int) (this.w*this.tile_sz), (int) (this.w*this.tile_sz)));
	}
	
	private List<Obstacle> obstacles;
	protected List<Obstacle> getObstacles() { return obstacles; };
	public void addObstacle(Obstacle o) { obstacles.add(o); }
	public void removeObstacle(Point pos) {
		final int idx = findObstacle(pos);
		if(idx >= 0) obstacles.remove(idx);
	}
	
	public int findObstacle(Point pos) {
		int idx = 0;
		for(Obstacle o : obstacles) {
			if(o.collision(pos)) break;
			idx++;
		}
		if(idx < obstacles.size()) return idx;
		return -1;
	}
	public boolean collision(Obstacle target_obstacle) {
		final Point p = target_obstacle.getPos();
		if(p.x < 0 || p.x >= w || p.y < 0 || p.y >= h) return true;
		
		for(Obstacle o : obstacles)
			if(o.collision(target_obstacle)) return true;
		return false;
	}
	
	//Convertit la position du point en le # de colonne/ligne correspondant
	public Point toGridPos(Point coord) {
		return new Point((int) (coord.x/this.tile_sz), (int) (coord.y/this.tile_sz));
	}
	
	//Convertit # colonne/ligne en coordonnees x/y
	public Point toCoord(Point pos) {
		return new Point(this.tile_sz * pos.x, this.tile_sz * pos.y);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(Color.GRAY);
		
		//Taille quadrillage, min. entre taille canvas et taille monde
		final int xmax = Math.min(getWidth(), this.w*this.tile_sz);
		final int ymax = Math.min(getHeight(), this.h*this.tile_sz);

		//On dessine le quadrillage
		for(int i = 0; i < this.w+1; i++)
			for(int j = 0; j < this.h+1; j++) {
				g.drawLine(0, j*this.tile_sz, xmax, j*this.tile_sz);
				g.drawLine(i*this.tile_sz, 0, i*this.tile_sz, ymax);
			}

		//On dessine les obstacles
		for(Obstacle o : obstacles) {
			final Point pos = toCoord(o.getPos());
			final boolean[][] shape = o.shape();
			for(int i = 0; i < shape.length; i++)
				for(int j = 0; j < shape[0].length; j++) {
					if(shape[i][j]) {
						final Point rel = toCoord(new Point(i, j));
						g.drawImage(o.getImage(), pos.x+rel.x, pos.y+rel.y, null);
					}
				}
		}
	}
	
	public boolean[][] shape() {
		boolean[][] mat = new boolean[this.w][this.h];
		for(int i = 0; i < this.w; i++)
			for(int j = 0; j < this.h; j++)
				mat[i][j] = (-1 != this.findObstacle(new Point(i, j)));
		return mat;
	}
	
	protected void clearObstacles() {
		obstacles = new LinkedList<Obstacle>();
	}
}
