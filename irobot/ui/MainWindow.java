package irobot.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import irobot.input.StateAction;
import irobot.logic.Robot;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.BoxLayout;
import java.awt.Color;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;

public class MainWindow {
	private ObstaclesPanel obstaclesPanel;
	private Map map;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//global
		frame = new JFrame();
		frame.setTitle("iRobot");
		frame.setBounds(100, 100, 1200, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//obstacles panel
		obstaclesPanel = new ObstaclesPanel();
		obstaclesPanel.setBackground(Color.DARK_GRAY);
		frame.getContentPane().add(obstaclesPanel, BorderLayout.EAST);
		obstaclesPanel.setLayout(new BoxLayout(obstaclesPanel, BoxLayout.Y_AXIS));
		
		//map
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		UIManager.put("ScrollBar.background", Color.GRAY);
		
		map = new Map(obstaclesPanel);
		map.setBackground(Color.DARK_GRAY);
		scrollPane.setViewportView(map);
		
		//tools panel
		JPanel toolsPane = new JPanel();
		toolsPane.setBackground(Color.DARK_GRAY);
		frame.getContentPane().add(toolsPane, BorderLayout.WEST);
		
		JButton btnAddObject = new JButton("Placer obstacle");
		btnAddObject.setHorizontalAlignment(SwingConstants.LEADING);
		btnAddObject.setIcon(new ImageIcon("src/images/add.png"));
		btnAddObject.addActionListener(new StateAction(map, Map.State.PLACE_OBJECT));
		toolsPane.setLayout(new MigLayout("", "[200px]", "[80px][80px][80px][80px][80px][80px][116px]"));
		toolsPane.add(btnAddObject, "cell 0 0,grow");
		
		JButton btnRemoveObject = new JButton("Enlever obstacle");
		btnRemoveObject.setHorizontalAlignment(SwingConstants.LEADING);
		btnRemoveObject.setIcon(new ImageIcon("src/images/remove.png"));
		btnRemoveObject.addActionListener(new StateAction(map, Map.State.REMOVE_OBJECT));
		toolsPane.add(btnRemoveObject, "cell 0 1,grow");
		
		JButton btnEndPoint = new JButton("Arrivée");
		btnEndPoint.setHorizontalAlignment(SwingConstants.LEADING);
		btnEndPoint.setIcon(new ImageIcon("src/images/endpt.png"));
		btnEndPoint.addActionListener(new StateAction(map, Map.State.PLACE_END_PT));
		
		JButton btnStartPoint = new JButton("Départ");
		btnStartPoint.setHorizontalAlignment(SwingConstants.LEADING);
		btnStartPoint.setIcon(new ImageIcon("src/images/startpt.png"));
		btnStartPoint.addActionListener(new StateAction(map, Map.State.PLACE_START_PT));
		
		JButton btnFreeDraw = new JButton("Tracé libre");
		btnFreeDraw.setHorizontalAlignment(SwingConstants.LEADING);
		btnFreeDraw.setIcon(new ImageIcon("src/images/draw.png"));
		btnFreeDraw.addActionListener(new StateAction(map, Map.State.FREE_DRAW));
		toolsPane.add(btnFreeDraw, "cell 0 2,grow");
		toolsPane.add(btnStartPoint, "cell 0 3,grow");
		toolsPane.add(btnEndPoint, "cell 0 4,grow");
		
		JButton btnStart = new JButton("Simulation");
		btnStart.setHorizontalAlignment(SwingConstants.LEADING);
		btnStart.setIcon(new ImageIcon("src/images/play.png"));
		btnStart.addActionListener(new StateAction(map, Map.State.SIMULATING) {
			@Override
			public void process() {
				final Robot r = obstaclesPanel.getCurrentRobot();
				if(r == null) {
					JOptionPane.showMessageDialog(frame, "Pas de robot sélectionné");
					getParent().setState(Map.State.IDLE);
					return;
				}
				
				map.setRobot(r);
				r.startSimulation(map);
			}
			@Override
			public void cancel() {
				
			}
		});
		toolsPane.add(btnStart, "cell 0 5,grow");
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setForeground(Color.LIGHT_GRAY);
		lblLogo.setIcon(new ImageIcon("src/images/logo.png"));
		toolsPane.add(lblLogo, "cell 0 6");
		
		//menu bar
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("Fichier");
		menuBar.add(mnFile);
		
		JMenuItem mntmExit = new JMenuItem("Quitter");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		JMenuItem mntmSave = new JMenuItem("Sauvegarder");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				JFileChooser fc = new JFileChooser();
				if(fc.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
					File f = fc.getSelectedFile();
					map.save(f);
				}
			}
		});
		mnFile.add(mntmSave);
		
		JMenuItem mntmLoad = new JMenuItem("Charger");
		mntmLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				JFileChooser fc = new JFileChooser();
				if(fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
					File f = fc.getSelectedFile();
					map.load(f);
				}
			}
		});
		mnFile.add(mntmLoad);
		
		JSeparator separator = new JSeparator();
		mnFile.add(separator);
		mnFile.add(mntmExit);
	}
}
