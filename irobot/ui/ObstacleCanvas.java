package irobot.ui;

import irobot.input.ObstacleEditorMouseListener;

public class ObstacleCanvas extends GridCanvas {
	private static final long serialVersionUID = 1L;
	
	public enum State { IDLE, DRAW, ERASE }
	private State state;
	public State getState() { return state; }
	public void setState(State s) { state = s; }
	
	public ObstacleCanvas() {
		super();
		
		w = 20; h = 20;
		
		initialize();
	}
	
	@Override
	protected void initialize() {
		ObstacleEditorMouseListener input = new ObstacleEditorMouseListener(this);
		this.addMouseListener(input);
		this.addMouseMotionListener(input);
		
		super.initialize();
	}
}
