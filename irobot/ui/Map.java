package irobot.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import irobot.input.MapMouseListener;
import irobot.logic.Obstacle;
import irobot.logic.Point;
import irobot.logic.Robot;

public class Map extends GridCanvas {
	private static final long serialVersionUID = 1L;

	public enum State {
		IDLE, PLACE_OBJECT, REMOVE_OBJECT,
		PLACE_START_PT, PLACE_END_PT, FREE_DRAW,
		SIMULATING, 
	}
	
	public void setStartPt(Point p) { start_obs.move(p);}
	public Point getStartPt() { return start_obs.getPos(); }
	public void setEndPt(Point p) { end_obs.move(p); }
	public Point getEndPt() { return end_obs.getPos(); }
	
	private Obstacle start_obs = Obstacle.start.copy(), end_obs = Obstacle.end.copy();
	private ObstaclesPanel obstaclesPanel;
	
	private State state;
	public State getState() { return this.state; }
	public void setState(State state) { this.state = state; }
	
	public Map(ObstaclesPanel op) {
		super();
		this.obstaclesPanel = op;
		state = State.IDLE;
		
		w = 75; h = 100;
		
		this.initialize();
	}
	
	protected void initialize() {
		MapMouseListener input = new MapMouseListener(this, obstaclesPanel);
		this.addMouseListener(input);
		this.addMouseMotionListener(input);
		
		super.initialize();
	}
	
	//Convertit la position du point en le # de colonne/ligne correspondant
	public Point toGridPos(Point coord) {
		return new Point((int) (coord.x/this.tile_sz), (int) (coord.y/this.tile_sz));
	}
	
	//Convertit # colonne/ligne en coordonnees x/y
	public Point toCoord(Point pos) {
		return new Point(this.tile_sz * pos.x, this.tile_sz * pos.y);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		//On dessine le robot
		if(state == State.SIMULATING) {
			//dessin trajectoire
			g.setColor(Color.LIGHT_GRAY);
			final List<Point> path = robot.getPath();
			final int off = tile_sz/2;
			for(int i = 0; i < path.size() - 1; i++) {
				final Point p1 = path.get(i), p2 = path.get(i+1);
				g.drawLine(p1.x+off, p1.y+off, p2.x+off, p2.y+off);
			}
			
			//dessin robot
			final Point p = robot.getCoord();
			g.drawImage(Obstacle.robot.getImage(), p.x, p.y, null);
		}

		//On dessine les objets spéciaux
		final Point start_pos = toCoord(start_obs.getPos());
		final Point end_pos = toCoord(end_obs.getPos());
		if(!start_pos.equals(new Point(-1, -1)))
			g.drawImage(start_obs.getImage(), start_pos.x, start_pos.y, null);
		if(!end_pos.equals(new Point(-1, -1)))
			g.drawImage(end_obs.getImage(), end_pos.x, end_pos.y, null);
	}
	
	private Robot robot;
	public void setRobot(Robot r) { robot = r; }
	
	public void save(File f) {
		try {
			FileWriter fw = new FileWriter(f);
			fw.write(String.valueOf(w) + "\r\n");
			fw.write(String.valueOf(h) + "\r\n");
			fw.write(String.valueOf(start_obs.getPos().x) + "\r\n");
			fw.write(String.valueOf(start_obs.getPos().y) + "\r\n");
			fw.write(String.valueOf(end_obs.getPos().x) + "\r\n");
			fw.write(String.valueOf(end_obs.getPos().y) + "\r\n");
			
			for(Obstacle o : this.getObstacles()) {
				final Point p = o.getPos();
				fw.write(o.getName() + "\r\n");
				fw.write(p.x + "\r\n");
				fw.write(p.y + "\r\n");
			}
			
			fw.close();
		} catch (IOException exception) {
			System.out.println("Erreur lors de l'écriture : " + exception.getMessage());
		}
	}
	
	public void load(File f) {
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			
			this.w = Integer.valueOf(br.readLine());
			this.h = Integer.valueOf(br.readLine());

			final int sx = Integer.valueOf(br.readLine());
			final int sy = Integer.valueOf(br.readLine());
			final int ex = Integer.valueOf(br.readLine());
			final int ey = Integer.valueOf(br.readLine());

			this.setStartPt(new Point(sx, sy));
			this.setEndPt(new Point(ex, ey));
			
			this.clearObstacles();
			
			String name = null; Obstacle o = null;
			while((name = br.readLine()) != null) {
				if(name.equals("square")) o = Obstacle.square;
				else {
					o = obstaclesPanel.findObstacle(name);
					if(o == null) {
						br.readLine(); br.readLine();
						System.out.println("Obstacle " + name + " inexistant.");
						continue;
					}
				}

				final int x = Integer.valueOf(br.readLine());
				final int y = Integer.valueOf(br.readLine());
				this.addObstacle(o.copy(new Point(x, y)));
			}
			
			fr.close();
		} catch (IOException exception) {
			System.out.println("Erreur lors de la lecture : " + exception.getMessage());
		}
		
		this.repaint();
	}
}
