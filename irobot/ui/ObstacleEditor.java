package irobot.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;

import irobot.logic.Obstacle;
import irobot.logic.Point;

public class ObstacleEditor {

	private JFrame frame;
	private JTextField txtObstacle;
	
	private ObstaclesPanel obstaclesPanel;
	private ObstacleCanvas canvas;

	public static void open(ObstaclesPanel op, Obstacle o) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ObstacleEditor e = new ObstacleEditor(op);
					
					if(o != null) {
						e.txtObstacle.setText(o.getName());
						
						boolean[][] mat = o.shape();
						for(int i = 0; i < mat.length; i++)
							for(int j = 0; j < mat[0].length; j++) {
								final Point p = new Point(i, j);
								if(mat[i][j])
									e.canvas.addObstacle(Obstacle.square.copy(p));
							}
					}
					
					e.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ObstacleEditor(ObstaclesPanel op) {
		this.obstaclesPanel = op;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("\u00C9diteur d'obstacles");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		canvas = new ObstacleCanvas();
		scrollPane.setViewportView(canvas);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new MigLayout("", "[136px,grow][136px][136px]", "[25px][25px]"));
		
		JButton btnDraw = new JButton("Tracer");
		btnDraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				if(canvas.getState() != ObstacleCanvas.State.DRAW)
					canvas.setState(ObstacleCanvas.State.DRAW);
				else
					canvas.setState(ObstacleCanvas.State.IDLE);
			}
		});
		panel.add(btnDraw, "cell 0 0,grow");
		
		JButton btnErase = new JButton("Effacer");
		btnErase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				if(canvas.getState() != ObstacleCanvas.State.ERASE)
					canvas.setState(ObstacleCanvas.State.ERASE);
				else
					canvas.setState(ObstacleCanvas.State.IDLE);
			}
		});
		panel.add(btnErase, "cell 1 0,grow");
		
		txtObstacle = new JTextField();
		panel.add(txtObstacle, "cell 0 1 2 1,growx");
		txtObstacle.setColumns(10);
		
		JButton btnSave = new JButton("Sauvegarder");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				final Obstacle o = new Obstacle(canvas.shape(), txtObstacle.getText());
				obstaclesPanel.addObstacle(o);
			}
		});
		panel.add(btnSave, "cell 2 1,grow");
	}
}
