package irobot.input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import irobot.logic.Obstacle;
import irobot.logic.Point;
import irobot.ui.Map;
import irobot.ui.ObstaclesPanel;

public class MapMouseListener implements MouseListener, MouseMotionListener {
	private Map parent;
	private ObstaclesPanel obstaclesPanel;
	public MapMouseListener(Map parent, ObstaclesPanel op) {
		this.parent = parent;
		this.obstaclesPanel = op;
	}
	
	public void mouseDragged(MouseEvent e) {
		final Point pos = parent.toGridPos(new Point(e.getPoint()));
		
		switch(parent.getState()) {
			case FREE_DRAW: {
				final Obstacle o = Obstacle.square.copy(pos);
				if(!parent.collision(o))
					parent.addObstacle(o);
			} break;

			case REMOVE_OBJECT:
				parent.removeObstacle(pos);
			break;
			default: break;
		}
		
		parent.repaint();
	}
	public void mouseMoved(MouseEvent e) {}
	
	public void mouseClicked(MouseEvent e) {
		final Point pos = parent.toGridPos(new Point(e.getPoint()));
		
		switch(parent.getState()) {
		case PLACE_OBJECT: {
			final Obstacle co = obstaclesPanel.getCurrentObstacle();
			if(co != null) {
				final Obstacle o = co.copy(pos);
				if(!parent.collision(o))
					parent.addObstacle(o);
			}
		} break;
		case PLACE_START_PT: {
			final Obstacle sq = Obstacle.square.copy(pos);
			if(!parent.collision(sq)) parent.setStartPt(pos);
		} break;
		case PLACE_END_PT: {
			final Obstacle sq = Obstacle.square.copy(pos);
			if(!parent.collision(sq)) parent.setEndPt(pos);
		} break;
		case FREE_DRAW: {
			final Obstacle o = Obstacle.square.copy(pos);
			if(!parent.collision(o))
				parent.addObstacle(o);
		} break;
		
		case REMOVE_OBJECT:
			parent.removeObstacle(pos);
		break;
		default: break;
		}
		
		parent.repaint();
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	
}
