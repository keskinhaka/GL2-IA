package irobot.input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.event.MouseInputListener;

import irobot.logic.Obstacle;
import irobot.logic.Point;
import irobot.ui.ObstacleCanvas;

public class ObstacleEditorMouseListener implements MouseListener, MouseInputListener{
	private ObstacleCanvas parent;
	public ObstacleEditorMouseListener(ObstacleCanvas parent) {
		this.parent = parent;
	}
	
	public void mouseDragged(MouseEvent e) {
		final Point pos = parent.toGridPos(new Point(e.getPoint()));
		
		switch(parent.getState()) {
			case DRAW: {
				final Obstacle o = Obstacle.square.copy(pos);
				if(!parent.collision(o))
					parent.addObstacle(o);
			} break;

			case ERASE:
				parent.removeObstacle(pos);
			break;
			default: break;
		}
		
		parent.repaint();
	}
	public void mouseMoved(MouseEvent e) {}
	
	public void mouseClicked(MouseEvent e) {
		final Point pos = parent.toGridPos(new Point(e.getPoint()));
		
		switch(parent.getState()) {
		case DRAW: {
			final Obstacle o = Obstacle.square.copy(pos);
			if(!parent.collision(o))
				parent.addObstacle(o);
		} break;
		
		case ERASE:
			parent.removeObstacle(pos);
		break;
		default: break;
		}
		
		parent.repaint();
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
}
