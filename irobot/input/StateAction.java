package irobot.input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import irobot.ui.Map;

public class StateAction implements ActionListener {
		private Map parent;
		private Map.State state;
		
		public Map getParent() { return parent; }
		public Map.State getState() { return state; }
		
		public StateAction(Map parent, Map.State state) {
			this.parent = parent;
			this.state = state;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			if(parent.getState() == state) {
				parent.setState(Map.State.IDLE);
				cancel();
			} else {
				parent.setState(state);
				process();
			}
		}
		
		public void cancel() {}
		public void process() {}
	}