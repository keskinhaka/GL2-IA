package irobot.logic;

import irobot.logic.Point;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import irobot.ui.Map;

public class Robot {
	final static private float speed = 7f/100f;

	Map parent;
	protected boolean[][] map;
	protected List<Point> path;
	
	public List<Point> getPath() { return path; }
	
	final static protected Point STOP = new Point(-1, -1);
	
	public Robot() {
	}
	
	private boolean walkable(int x, int y) {
		if(x < 0 || y < 0) return false;
		if(x >= map.length || y >= map[0].length) return false;
		return !map[x][y];
	}
	
	protected List<Point> neighbors(Point p) {
		List<Point> ret = new ArrayList<Point>();

		//Diagonales
		if(walkable(p.x-1, p.y-1) && walkable(p.x-1, p.y) && walkable(p.x, p.y-1))
			ret.add(new Point(p.x-1, p.y-1));
		if(walkable(p.x+1, p.y-1) && walkable(p.x+1, p.y) && walkable(p.x, p.y-1))
			ret.add(new Point(p.x+1, p.y-1));
		if(walkable(p.x-1, p.y+1) && walkable(p.x-1, p.y) && walkable(p.x, p.y+1))
			ret.add(new Point(p.x-1, p.y+1));
		if(walkable(p.x+1, p.y+1) && walkable(p.x+1, p.y) && walkable(p.x, p.y+1))
			ret.add(new Point(p.x+1, p.y+1));
		
		//Haut bas gauche droite
		if(walkable(p.x-1, p.y)) ret.add(new Point(p.x-1, p.y));
		if(walkable(p.x+1, p.y)) ret.add(new Point(p.x+1, p.y));
		if(walkable(p.x, p.y-1)) ret.add(new Point(p.x, p.y-1));
		if(walkable(p.x, p.y+1)) ret.add(new Point(p.x, p.y+1));
		
		return ret;
	}
	
	protected void buildPath(Point a, Point b) {
		Queue<Point> Q = new LinkedList<Point>(); //à visiter
		List<Point> visited = new ArrayList<Point>(); //déjà visités
		float[][] dist = new float[map.length][map[0].length];
		Point[][] prev = new Point[map.length][map[0].length];
		
		for(int i = 0; i < map.length; i++)
			for(int j = 0; j < map[0].length; j++) {
				dist[i][j] = Float.POSITIVE_INFINITY;
				prev[i][j] = Robot.STOP;
			}
		
		dist[a.x][a.y] = 0;
		Q.add(a);
		
		Point p;
		while(!Q.isEmpty())
		{
			p = Q.poll();
			if(visited.contains(p)) continue;
			visited.add(p);
			
			for(Point v : neighbors(p)) {
				final float alt = dist[p.x][p.y] + (float) p.distance(v);
				if(alt < dist[v.x][v.y]) {
					dist[v.x][v.y] = alt;
					prev[v.x][v.y] = p;
				}
				
				Q.add(v);
			}
		}

		this.path = new LinkedList<Point>();
		p = b;
		while(!p.equals(Robot.STOP)) {
			path.add(p);
			p = prev[p.x][p.y];
		}
		Collections.reverse(path);
		if(!path.get(0).equals(a)) {
			JOptionPane.showMessageDialog(parent, "Chemin impossible");
			this.path = new LinkedList<Point>();
			this.path.add(a);
		}
		for(int i = 0; i < path.size(); i++)
			path.set(i, parent.toCoord(path.get(i)));
	}
	
	private long t; //Timestamp de la dernière màj de la position du robot
	private float x, y; //Positions du robot
	private int i; //# du dernier jalon du chemin franchi
	private Timer timer; //Gere la vitesse d'animation
	public void startSimulation(Map parent) {
		this.parent = parent;
		this.map = parent.shape();
		this.buildPath(parent.getStartPt(), parent.getEndPt());
		
		t = System.currentTimeMillis();
		i = 0;
		
		final Point start = path.get(0);
		x = start.x; y = start.y;
		
		parent.repaint();
		
		
		timer = new Timer(20, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				parent.repaint();
			}
		});
		
		timer.start();
	}
	public Point getCoord() {
		if(i == path.size() - 1)
			return new Point((int) x, (int) y);
		
		final long t2 = System.currentTimeMillis();
		long delta_t = t2 - t;
		
		//On saute de jalon en jalon tant qu'on a de la marge
		//(en comparant temps restant et distance à effectuer)
		Point next = path.get(i+1);
		
		while(delta_t*speed >= next.distance(x, y)) {
			x = next.x; y = next.y;
			delta_t -= next.distance(x, y)/speed;
			
			i += 1;
			if(i == path.size() - 1) {
				if(parent != null) {
					parent.setState(Map.State.IDLE);
					EventQueue.invokeLater(new Runnable() {
						public void run() { parent.repaint(); }
					});
				}
				timer.stop();
				return new Point((int) x, (int) y);
			}
			next = path.get(i+1);
		}
		
		//Calcul du dernier segment (non complet) effectué
		Point direction = path.get(i+1).difference(path.get(i));
		final float l = (float) direction.distance(0,0);
		final Point2D.Float p = direction.times(delta_t*speed/l);
		x += p.x; y += p.y;
		
		//On met à jour le dernier instant animé
		t = System.currentTimeMillis();
		
		return new Point((int) x, (int) y);
	}
	
	@Override
	public String toString() {
		return "Robot optimal";
	}
}
