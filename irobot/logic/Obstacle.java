package irobot.logic;

import irobot.logic.Point;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Obstacle {
	private String nom;
	private boolean[][] shape;
	private Image image;
	private Point pos;

	private static final boolean[][] _mat_1x1 = {{true}};
	public final static Obstacle square = new Obstacle(_mat_1x1, "square");
	public final static Obstacle start = new Obstacle(_mat_1x1, "start", "start.png");
	public final static Obstacle end = new Obstacle(_mat_1x1, "end", "end.png");
	public final static Obstacle robot = new Obstacle(_mat_1x1, "robot", "robot.png");
	
	public boolean[][] shape() { return shape; }
	public Image getImage() { return image; }
	public Point getPos() { return pos; }
	public void move(Point p) { pos.setLocation(p); } 

	
	public Obstacle(boolean[][] mat) {
		init(mat, "", "default.png");
	}
	public Obstacle(boolean[][] mat, String name) {
		init(mat, name, "default.png");
	}
	public Obstacle(boolean[][] mat, String name, String im_name) {
		init(mat, name, im_name);
	}
	
	private void init(boolean[][] mat, String name, String im_name) {
		shape = mat;
		this.nom = name;
		
		try {
			image = ImageIO.read(new File("src/images/" + im_name))
						.getScaledInstance(20, 20,Image.SCALE_DEFAULT);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.pos = new Point(-1, -1);
	}
	
	public Obstacle copy(Point pos) {
		Obstacle o = new Obstacle(shape, nom);
		o.image = this.image;
		
		o.move(pos);
		return o;
	}
	
	public Obstacle copy() {
		return this.copy(new Point(-1, -1));
	}
	
	public boolean collision(Point target_pos) {
		final Point comp = new Point(target_pos.x - pos.x, target_pos.y - pos.y);
		if(comp.x < 0 || comp.y < 0 || comp.x >= shape.length || comp.y >= shape[0].length)
			return false;
		else return shape[comp.x][comp.y];
	}
	
	public boolean collision(Obstacle o) {
		final Point opos = o.getPos();
		boolean[][] mat = o.shape();
		for(int i = 0; i < mat.length; i++)
			for(int j = 0; j < mat[0].length; j++)
				if( mat[i][j] && collision(new Point(opos.x+i, opos.y+j)) ) return true;
		return false;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
	public String getName() {
		return nom;
	}

}
