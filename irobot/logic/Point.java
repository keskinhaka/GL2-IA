package irobot.logic;

import java.awt.geom.Point2D;

public class Point extends java.awt.Point {
	private static final long serialVersionUID = 3185632872673217353L;

	public Point(int x, int y) {
		this.x = x; this.y = y;
	}
	
	public Point(Point other) {
		this.x = other.x;
		this.y = other.y;
	}
	
	public Point(java.awt.Point other) {
		this.x = other.x;
		this.y = other.y;
	}
	
	public Point difference(Point other) {
		return new Point(this.x - other.x, this.y - other.y);
	}
	
	public Point2D.Float times(float a) {
		return new Point2D.Float(a*x, a*y);
	}
}
