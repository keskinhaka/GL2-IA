package irobot.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.JOptionPane;

public class RobotTwo extends Robot {
	public RobotTwo() {
		super();
	}
	
	@Override
	protected void buildPath(Point a, Point b) {
		Queue<Point> Q = new LinkedList<Point>(); //à visiter
		List<Point> visited = new ArrayList<Point>(); //déjà visités
		float[][] dist = new float[map.length][map[0].length];
		Point[][] prev = new Point[map.length][map[0].length];
		
		for(int i = 0; i < map.length; i++)
			for(int j = 0; j < map[0].length; j++) {
				dist[i][j] = Float.POSITIVE_INFINITY;
				prev[i][j] = Robot.STOP;
			}
		
		dist[a.x][a.y] = 0;
		Q.add(a);
		
		Point p;
		while(!Q.isEmpty())
		{
			p = Q.poll();
			if(visited.contains(p)) continue;
			visited.add(p);
			
			for(Point v : neighbors(p)) {
				final float alt = (float) p.distance(v);
				if(alt < dist[v.x][v.y]) {
					dist[v.x][v.y] = alt;
					prev[v.x][v.y] = p;
				}
				
				Q.add(v);
			}
		}

		this.path = new LinkedList<Point>();
		p = b;
		while(!p.equals(Robot.STOP)) {
			path.add(p);
			p = prev[p.x][p.y];
		}
		Collections.reverse(path);
		if(!path.get(0).equals(a)) {
			JOptionPane.showMessageDialog(parent, "Chemin impossible");
			this.path = new LinkedList<Point>();
			this.path.add(a);
		}
		for(int i = 0; i < path.size(); i++)
			path.set(i, parent.toCoord(path.get(i)));
	}
	
	@Override
	public String toString() {
		return "Robot orthogonal";
	}
}
